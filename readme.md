# What is TocoPos

TocoPos adalah ...


# Library Use

* [Template](http://jeromejaglale.com/doc/php/codeigniter_template)
* [HMVC](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/src/codeigniter-3.x)
* [Ion Auth](https://github.com/benedmunds/CodeIgniter-Ion-Auth)


# Server Requirements

PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.


# Installation

Please see the `installation section [HMVC](https://codeigniter.com/user_guide/installation/index.html)` of the CodeIgniter User Guide.


# License

Please see the `license`
agreement ...


# Acknowledgement

The TocoPos team ...


# Folder Structure CI-HMVC

```

codeigniter/
├── application/
│   └── modules
│       ├── folder_a
│       │   ├── controllers
│       │   ├── models
│       │   └── views
│       └── folder_b
│           ├── controllers
│           ├── models
│           └── views
├── system/
└── assets/

```












