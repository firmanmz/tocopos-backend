<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<?php if($this->ion_auth->logged_in() ){ ?>
		<meta http-equiv="refresh" content="500;url=<?php //echo site_url('logout');?>" />
		<?php } ?>
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
		<title><?php echo $title; ?></title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="robots" content="noindex, nofollow">
		
		<!-- Bootstrap 3.3.7 -->
		<link rel="stylesheet" href="<?php echo base_url('assets/backend/base/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo base_url('assets/backend/base/font-awesome/css/font-awesome.min.css');?>">
		
		<!-- Theme style -->
		<?php $this->load->view('su_auth/header/header-other/other-theme'); ?>
		<!-- bootstrap validator -->
		<?php $this->load->view('su_auth/header/header-other/other-validator'); ?>
		<!-- Nprogress -->
		<?php $this->load->view('su_auth/header/header-other/other-nprogress'); ?>

		<!-- master css -->
		<link rel="stylesheet" href="<?php echo base_url('assets/backend/base/master.css');?>">

  		<!-- Google Font -->
  		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  		<!-- Google Font -->
  		<!--<link rel="stylesheet" href="<?php echo base_url('assets/backend/plugins/google-fonts/google-fonts.css');?>">-->

		<!-- iCheck -->
		<link rel="stylesheet" href="<?php echo base_url('assets/backend/plugins/iCheck/square/blue.css');?>">
	</head>