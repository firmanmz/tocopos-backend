<?php
		if($url2=="create_user_staff" or $url2=="update_user_staff"){$var = site_url('manage/users/staff');}
		elseif($url2=="create_user_member" or $url2=="update_user_member"){$var = site_url('manage/users/member');}
		elseif($url2=="add_group" or $url2=="update_group" or $url2=="group_permissions"){ $var = site_url('manage/groups');}
		elseif($url2=="add_permission" or $url2=="update_permission"){ $var = site_url('manage/permissions');}
		elseif($url2=="add_bank" or $url2=="update_bank"){ $var = site_url('manage/bank');}
		elseif($url2=="add_hotel" or 
			   $url2=="update_hotel" or 
			   $url2=="add_hotel_room" or 
			   $url2=="update_hotel_room" or 
			   $url2=="hotel_room"){ $var = site_url('manage/hotel');}
		elseif($url2=="add_packet" or $url2=="update_packet"){ $var = site_url('manage/packet');}
		elseif($url2=="add_commission_formula" or $url2=="update_commission_formula"){ $var = site_url('manage/commission/formula');}
		elseif($url2=="report_member_voucher_rest"){ $var = site_url('manage/users/member');}
		
		echo 
		'<a href="'.$var.'" class="pull-left btn btn-warning btn-flat btn-arrow-left">
		<i class="fa fa-reply fa-fw"></i> Back
		</a>';
		$pr = "pull-right";
		$bb = "";
	
	$save = array(
		'name' => 'save',
		'id' => 'save',
		'value' => 'true',
		'type' => 'submit',
		'class' => $pr.' btn btn-primary btn-flat btn-save '.$bb,
		'content' => '<i class="fa fa-floppy-o fa-fw"></i>'.lang('all_save_btn'),
		//'disabled' => 'disabled',
	);
	if($url2!="hotel_room"){
		echo form_button($save);
	}
?>