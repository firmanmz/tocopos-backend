<body class="hold-transition login-page" style='display: none'>
	<div class='fade out'>
		<div class="login-box">
			<div class="login-logo">
				<a href="<? echo site_url();?>"><b><?php echo lang('login_heading');?></b><? echo lang('login_ending')?></a>
			</div><!-- /.login-logo -->
			<div class="login-box-body">
				<p class="login-box-msg"><?php echo lang('login_subheading');?></p>
				<?php $this->load->view('su_auth/component/elemen/elemen-message'); ?>
				
				<?php $attribute= array( 'autocomplete' => "off", 'id'=>'reg_form','class' => 'reg_form'); 
				echo form_open($this->uri->uri_string(), $attribute);?>
					<div class="form-group has-feedback">
						<label><?php echo lang('login_identity_label');?></label>
						<?php echo form_input($identity);?>
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
					 	<div class="help-block with-errors"></div>
					</div>
					<div class="form-group has-feedback">
						<label><?php echo lang('login_password_label');?></label>
						<?php echo form_input($password);?>
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
					 	<div class="help-block with-errors"></div>
					</div>
					<div class="row">
						<div class="col-xs-8"><!--
							<a href="<?php echo site_url('forgot');?>" class="btn btn-warning btn-flat"><?php echo lang('login_forgot_password');?></a>
							
							<div class="checkbox icheck">
							<label>
								<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?><?php echo lang('login_remember_label', 'remember');?>
							</label>
							</div>
							-->
						</div><!-- /.col -->

						<!-- /.col -->
						<div class="col-xs-4">
							<button type="submit" class="btn btn-primary btn-block btn-flat"><?php echo lang('login_submit_btn');?></button>
						</div>
						<!-- /.col -->

					</div>
				<?php echo form_close();?>

				<a href="<? echo site_url('register');?>" class="text-center">Saya tidak punya akun</a>
			</div><!-- /.login-box-body -->
		</div><!-- /.login-box -->
    </div><!-- /.fade out -->