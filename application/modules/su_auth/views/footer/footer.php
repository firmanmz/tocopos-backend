	<!--<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>-->
	<script src="<?php echo base_url('assets/backend/plugins/jQuery/dist/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/backend/base/bootstrap/js/bootstrap.min.js');?>"></script>
	
	<!-- iCheck -->
	<script src="<? echo  base_url('assets/backend/plugins/iCheck/icheck.min.js')?>"></script>
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});
		});
	</script>
	

	<!-- Nprogress -->
	<?php $this->load->view('su_auth/footer/footer-other/other-nprogress'); ?>
	<!-- validation -->
	<?php $this->load->view('su_auth/footer/footer-other/other-validation'); ?>

	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/backend/base/adminlte/dist/js/adminlte.min.js');?>"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo base_url('assets/backend/base/adminlte/dist/js/demo.js');?>"></script>
	
	<?php $this->load->view('su_auth/footer/footer-other/noscript'); ?>
	
  </body>
</html>