<ul class="sidebar-menu tree" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>

    <li class="<?php if($url2 == "dashboard"){echo 'active';}?>">
		<a href="<?php echo site_url('dashboard'); ?>">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			<!--
			<span class="pull-right-container">
				<small class="label pull-right bg-green">new</small>
			</span>
			-->
		</a>
    </li>

     

	<!-- menu manage -->
	<?php if($this->ion_auth_acl->has_permission('menu_manage')){ ?>
	<li class="<?php if(
						$url2 == "manage" and (
						$url3 == "permissions" or 
						$url3 == "groups" or 
						$url3 == "users" or

						$url3 == "bank" or 
						$url3 == "hotel" or 
						$url3 == "packet" or 
						$url3 == "commission"

						)							
						){echo 'active';}?> treeview">
		<a href="#">
			<i class="fa fa-gears"></i>
			<span>Manage</span> 
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">

			<?php if($this->ion_auth_acl->has_permission('menu_manage_users_staff') or 
				     $this->ion_auth_acl->has_permission('menu_manage_users_staff_view')
			){ ?>
			<li class="<?php if($url3 == "users" and $url4 == "staff"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/users/staff'); ?>"><i class="fa fa-user-secret"></i> 
					<span>Staff</span> 
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_manage_users_member') or 
				     $this->ion_auth_acl->has_permission('menu_manage_users_member_view')
			){ ?>
			<li class="<?php if($url3 == "users" and $url4 == "member"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/users/member'); ?>"><i class="fa fa-user-circle"></i> 
					<span>Member</span> 
				</a>
			</li>
			<?php } ?>
			
			<!--
			<li class="treeview <?php if($url3 == "users"){echo 'active';}?>">
				<a  href="#"><i class="fa fa-users"></i> Users
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo site_url('manage/users/staff'); ?>"><i class="fa fa-user-secret"></i> Staff</a></li>
					<li><a href="<?php echo site_url('manage/users/member'); ?>"><i class="fa fa-user-circle"></i> Member</a></li>
				</ul>
			</li>
			-->

			<?php if($this->ion_auth_acl->has_permission('menu_manage_group') or 
				     $this->ion_auth_acl->has_permission('menu_manage_group_view')
			){ ?>
			<li class="<?php if($url3 == "groups"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/groups'); ?>"><i class="fa fa-th-large"></i> 
					<span>Group's</span> 
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_manage_permission') or 
				     $this->ion_auth_acl->has_permission('menu_manage_permission_view')
			){ ?>
			<li class="<?php if($url3 == "permissions" or $url2 == "update_permission"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/permissions'); ?>"><i class="fa fa-flag"></i> 
					<span>Permission's</span> 
				</a>
			</li>
			<?php } ?>

			<!--
			<?php if($this->ion_auth_acl->has_permission('menu_manage_bank') or 
				     $this->ion_auth_acl->has_permission('menu_manage_bank_view')
			){ ?>
			<li class="<?php if($url3 == "bank"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/bank'); ?>"><i class="fa fa-bank"></i> 
					<span>Bank</span> 
				</a>
			</li>
			<?php } ?>
			-->

			<?php if($this->ion_auth_acl->has_permission('menu_manage_hotel') or 
				     $this->ion_auth_acl->has_permission('menu_manage_hotel_view')
			){ ?>
			<li class="<?php if($url3 == "hotel"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/hotel'); ?>"><i class="fa fa-hotel"></i> 
					<span>Hotel</span> 
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_manage_packet') or 
				     $this->ion_auth_acl->has_permission('menu_manage_packet_view')
			){ ?>
			<li class="<?php if($url2 == "manage" and $url3 == "packet"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/packet'); ?>"><i class="fa fa-dropbox"></i> 
					<span>Packet</span> 
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_manage_commission_formula') or 
				     $this->ion_auth_acl->has_permission('menu_manage_commission_formula_view')
			){ ?>
			<li class="<?php if($url3 == "commission" and $url4 == "formula"){echo 'active';}?>">
				<a  href="<?php echo site_url('manage/commission/formula'); ?>"><i class="fa fa-flask"></i> 
					<span>Commission Formula</span> 
				</a>
			</li>
			<?php } ?>
		</ul>
	</li>
	<?php } ?>

	<!-- menu Transaction -->
	<?php if($this->ion_auth_acl->has_permission('menu_transaction')){ ?>
	<li class="<?php if(
						$url2 == "transaction" and  ( $url3 == "packet" or $url3 == "commission")							
						){echo 'active';}?> treeview">
		<a href="#">
			<i class="fa fa-reorder"></i>
			<span>Transaction</span> 
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<?php if($this->ion_auth_acl->has_permission('menu_transaction_packet') or 
				     $this->ion_auth_acl->has_permission('menu_transaction_packet_view')
			){ ?>
			<li class="<?php if($url2 == "transaction" and $url3 == "packet"){echo 'active';}?>">
				<a  href="<?php echo site_url('transaction/packet'); ?>"><i class="fa fa-dropbox"></i> 
					<span>Packet</span> 
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_transaction_commission') or 
				     $this->ion_auth_acl->has_permission('menu_transaction_commission_view')
			){ ?>
			<li class="<?php if($url2 == "transaction" and $url3 == "commission"){echo 'active';}?>">
				<a  href="<?php echo site_url('transaction/commission'); ?>"><i class="fa fa-money"></i> 
					<span>Commission</span> 
				</a>
			</li>
			<?php } ?>

		</ul>
	</li>
	<?php } ?>

	<!-- menu Transaction -->
	<?php if($this->ion_auth_acl->has_permission('menu_reservation')){ ?>
	<li class="<?php if(
						($url3 == "voucher" and $url3 == "non")	or	
						($url3 == "voucher")				
						){echo 'active';}?> treeview">
		<a href="#">
			<i class="fa fa-tty"></i>
			<span>Reservation</span> 
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">


			<?php if($this->ion_auth_acl->has_permission('menu_reservation_voucher_with') or 
				     $this->ion_auth_acl->has_permission('menu_reservation_voucher_with_view')
			){ ?>
			<li class="<?php if($url3 == "voucher" and $url4==""){echo 'active';}?>">
				<a  href="<?php echo site_url('reservation/voucher'); ?>">
					<span class="fa-stack">
						<i class="fa fa-ticket fa-rotate-270 fa-stack-1x"></i>
						<i class="fa fa-circle-o fa-stack-2x"></i>
					</span>
					<span>With-voucher</span> <? echo $this->m_reservation_voucher->get_num_reservation_voucher();?>
				</a>
			</li>
			<?php } ?>

			<?php if($this->ion_auth_acl->has_permission('menu_reservation_voucher_non') or 
				     $this->ion_auth_acl->has_permission('menu_reservation_voucher_non_view')
			){ ?>
			<li class="<?php if($url3 == "voucher" and $url4=="non"){echo 'active';}?>">
				<a  href="<?php echo site_url('reservation/voucher/non'); ?>">
					<span class="fa-stack">
						<i class="fa fa-ticket fa-rotate-270 fa-stack-1x"></i>
						<i class="fa fa-ban fa-stack-2x"></i>
					</span>
					<span>Non-voucher</span> 
				</a>
			</li>
			<?php } ?>
			
		</ul>
	</li>
	<?php } ?>

	<?php if($this->ion_auth_acl->has_permission('menu_setting')){ ?>
	<!-- menu setting -->
	<li class="<?php if(
						$url2 == "setting" and  ( $url3 == "general" or $url3 == "seo" or $url3 == "infoweb" or $url3 == "socmed")							
						){echo 'active';}?> treeview">
		<a href="#">
			<i class="fa fa-wrench"></i>
			<span>Setting</span> 
			<i class="fa fa-angle-left pull-right"></i>
		</a>
		<ul class="treeview-menu">
			<li class="<?php if($url3 == "general"){echo 'active';}?>">
				<a  href="<?php echo site_url('setting/general'); ?>"><i class="fa fa-universal-access"></i> 
					<span>General</span> 
				</a>
			</li>
			<li class="<?php if($url3 == "seo"){echo 'active';}?>">
				<a href="<?php echo site_url('setting/seo'); ?>"><i class="fa fa-globe"></i> 
					<span>SEO</span> 
				</a>
			</li>
			<li class="<?php if($url3 == "socmed"){echo 'active';}?>">
				<a  href="<?php echo site_url('setting/socmed'); ?>"><i class="fa fa-share-alt"></i> 
					<span>Social Media</span> 
				</a>
			</li>
		</ul>
	</li>
	<?php } ?>


	<?php if($this->ion_auth_acl->has_permission('menu_slider')){ ?>
	<!-- menu slider -->

    <li class="<?php if($url2 == "slide"){echo 'active';}?>">
		<a href="<?php echo site_url('slider/slide'); ?>">
			<i class="fa fa-image"></i> <span>Slider</span>
		</a>
    </li>
	<?php } ?>
	
	<!-- menu logs --
	<?php if($this->ion_auth_acl->has_permission('menu_logs')){ ?>
    <li class="<?php if(($url2 == "manage" and  ( $url3 == "log_activity" )) ){echo 'active';}?>">
		<a href="<?php echo site_url('manage/log_activity'); ?>">
			<i class="fa fa-history"></i> <span>Logs</span>
		</a>
    </li>
	<?php } ?>
	-->
	
	<!-- menu view website -->
    <li>
		<a href="<?php echo base_url(); ?>" target="_new">
			<i class="fa fa-external-link-square"></i> <span>View website</span>
		</a>
    </li>
		
</ul>
