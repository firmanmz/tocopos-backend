<footer class="main-footer">
  <div class="pull-right hidden-xs">
	<strong>Powered by <a target="_new" href="http://estocod.com/">Estocod.com</a>.</strong>
  </div>
  <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo site_url(); ?>"><?php echo $this->config->item('site_title','ion_auth');?></a>.</strong> All rights reserved. (Ver System. <?php echo CI_VERSION;?>)
</footer>