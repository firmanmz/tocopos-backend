<!-- User Account: style can be found in dropdown.less -->
<?php $get_my = $this->ion_auth->user($this->session->id)->row();?>
<li class="user user-menu">
	<a href="<?php echo site_url('update/profile/'.encodeID($get_my->id)); ?>" 
	   title="Edit profile <?php echo ucfirst(word_limiter($this->session->full_name,2)); ?>" 
	   class="active">
		<?php 
			$gravatar_url = $this->gravatar->get($this->session->email);
			if($this->gravatar->last_error()==false and is_connected()==true){
				if($gravatar_url){
					echo '<img style="font-size: 1em;width: 1.2em;border-radius:50%;" src="'.$gravatar_url.'" class="fa" alt="Edit profile '.ucfirst(word_limiter($this->session->full_name,2)).'" > ';
				}else{
					echo '<i class="fa fa-user-circle fa-lg fa-fw"></i>';
				}
			}else{
				echo '<i class="fa fa-user-circle fa-lg fa-fw"></i>';
			}
		?>
		<span class="hidden-xs">
			<?php echo ucfirst(word_limiter($this->session->full_name,2)); ?>
		</span>
	</a>
</li>
<!--
<li class="dropdown">
  	<a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Ganti bahasa">
	  	<i class="fa fa-language fa-lg fa-fw"></i>
		<span class="hidden-xs">
			<?php 
				if($this->session->userdata('site_lang')=="english"){ echo "EN"; }
				else{ echo "ID"; }
			?>
		</span>
		<span class="caret"></span>
	</a>
  	<ul class="dropdown-menu">
    	<li><a href="<?php echo base_url('language/english/'.encodeID(uri_string())); ?>"><i class="famfamfam-flag-gb"></i> English</a></li>
    	<li><a href="<?php echo base_url('language/indonesian/'.encodeID(uri_string())); ?>"><i class="famfamfam-flag-id"></i> Indonesian</a></li>
 	</ul>
</li>
-->
<li class="user user-menu">
	<a href="<?php echo site_url('logout');?>" title="Keluar dari sistem">
		<i class="fa fa-power-off fa-lg fa-fw"></i>
		<span class="hidden-xs">Logout</span>
	</a>
</li>