<?php if(!defined('BASEPATH')) exit('Hacking Attempt: Keluar dari sistem...!');
class M_num_ extends CI_Model{
	function __construct(){
		parent::__construct();
		if ($this->ion_auth->logged_in()){
			$user=$this->ion_auth->user()->row();
			$this->username=$user->username;
		}
	}
	
	//Jumlah Member
	public function get_num_member($status_member=false){
		$this->db->join('users_detail', 'users.id = users_detail.id_user', 'left');
		if($status_member=="1"){$this->db->where('users_detail.status_member',$status_member);} // member aktif
		elseif($status_member=="2"){$this->db->where('users_detail.status_member',$status_member);} // member tidak aktif
		$this->db->where('users.user_type',"02");
		$query = $this->db->get('users');
		return $query->num_rows();
	}
	//Jumlah reservasi dengan voucher
	public function get_num_reservation(){
		$query = $this->db->get('reservasi_hotel_voucher');
		return $query->num_rows();
	}
	//Jumlah reservasi tanpa voucher
	public function get_num_reservation_non(){
		$query = $this->db->get('reservasi_hotel_voucher_non');
		return $query->num_rows();
	}
	//Jumlah hotel yang tersedia dan sudah dibayar lunas -- dengan voucher
	public function get_num_hotel_available_lunas(){
		$this->db->join('log_duitku', 
		'log_duitku.merchantOrderId = reservasi_hotel_voucher.reservasiOrder', 'left'); 
		$this->db->where('log_duitku.resultCode',"00");
		$this->db->where('reservasi_hotel_voucher.status',"tersedia");
		$query = $this->db->get('reservasi_hotel_voucher');
		return $query->num_rows();
	}
	//Jumlah hotel yang tersedia dan sudah dibayar lunas -- tanpa voucher
	public function get_num_hotel_available_lunas_non(){
		$this->db->join('log_duitku', 
		'log_duitku.merchantOrderId = reservasi_hotel_voucher_non.reservasiOrder_non', 'left'); 
		$this->db->where('log_duitku.resultCode',"00");
		$this->db->where('reservasi_hotel_voucher_non.status',"tersedia");
		$query = $this->db->get('reservasi_hotel_voucher_non');
		return $query->num_rows();
	}
	public function get_num_packet_transaction($packet=false){
		$this->db->join('log_duitku', 
		'log_duitku.merchantOrderId = packet_transaction.packetOrderId', 'left'); 
		if($packet){
			$this->db->join('paket_detail', 'paket_detail.id_paket_detail = packet_transaction.id_detail_packet', 'left');
			//$this->db->join('packet', 'packet.id_packet = paket_detail.id_paket', 'left');	
			$this->db->where('paket_detail.id_paket',$packet);
		}
		$this->db->where('log_duitku.resultCode',"00");
		$query = $this->db->get('packet_transaction');
		return $query->num_rows();
	}
	public function get_num_packet_transaction_waiting($packet=false){
		$this->db->join('log_duitku', 
		'log_duitku.merchantOrderId = packet_transaction.packetOrderId', 'left'); 
		if($packet){
			$this->db->join('paket_detail', 'paket_detail.id_paket_detail = packet_transaction.id_detail_packet', 'left');
			//$this->db->join('packet', 'packet.id_packet = paket_detail.id_paket', 'left');	
			$this->db->where('paket_detail.id_paket',$packet);
		}
		$this->db->where('packet_transaction.cara_bayar_paket',"2");
		$this->db->where('log_duitku.resultCode',"03");
		$query = $this->db->get('packet_transaction');
		return $query->num_rows();
	}
}