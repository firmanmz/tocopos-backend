<? if($get_users){ foreach($get_users as $gu){ ?>
<? if(empty($gu->phone) and
      empty($gu->alamat) and
      empty($gu->id_provinces) and
      empty($gu->id_regencies) and
      empty($gu->id_districts)
     ){ ?>
<div class="box box-warning color-palette-box">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-warning"></i> Warning!</h3>
    </div>
    <div class="box-body">
        <p>
            Mohon kesediaan Anda untuk mengisi biodata diri, ini dapat mempercepat proses transaksi Anda.
            <div class="ui-group-buttons">
                <a href="<? echo site_url('update/profile/'.encodeID($id_user)); ?>" style="text-decoration:none;"
                title="Edit profile" class="btn btn-warning btn-lg btn-flat">
                    <i class="fa fa-edit fa-fw" ></i> Edit profile
                </a>
            </div>
        </p>
    </div>
    <!-- /.box-body -->
</div>
<? } } } ?>