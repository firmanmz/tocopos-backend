<!-- For Administrator -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_member(1);?></h3>
                <p>Jumlah Member Aktif</p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- /.col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_member(2);?></h3>
                <p>Jumlah Member Tidak Aktif</p>
            </div>
            <div class="icon">
                <i class="fa fa-user-times"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- /.col -->
</div>