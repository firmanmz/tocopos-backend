<!-- For Reservation -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_reservation()+$this->m_num_->get_num_reservation_non();?></h3>
                <p>Jumlah Reservasi Total</p>
            </div>
            <div class="icon">
                <span class="fa-stack" style="font-size:60px;">
                  <i class="fa fa-ticket fa-rotate-270 fa-stack-1x"></i>
                  <i class="fa fa-circle-o-notch fa-rotate-90 fa-stack-2x"></i>
                </span>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-maroon">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_reservation();?></h3>
                <p>Jumlah Reservasi with-Voucher</p>
            </div>
            <div class="icon">
                <span class="fa-stack" style="font-size:60px;">
                  <i class="fa fa-ticket fa-rotate-270 fa-stack-1x"></i>
                  <i class="fa fa-circle-o fa-stack-2x"></i>
                </span>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-maroon">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_reservation_non();?></h3>
                <p>Jumlah Reservasi non-Voucher</p>
            </div>
            <div class="icon">
                <span class="fa-stack" style="font-size:60px;">
                  <i class="fa fa-ticket fa-rotate-270 fa-stack-1x"></i>
                  <i class="fa fa-ban fa-stack-2x"></i>
                </span>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>