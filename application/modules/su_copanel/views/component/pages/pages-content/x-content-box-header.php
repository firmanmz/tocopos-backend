<div class="box-header">
	<h3 class="box-title">
		<!-- Pemberian nama pada tabel -->
		<?php if($url2 == "transaction" and $url3 == "packet"){ echo "Table Packet transactions".'<small> - '."packet small".'</small>'; ?>
		<?php }else if($url2 == "transaction" and $url3 == "commission"){ echo "Table Commission transactions".'<small> - '."commission small".'</small>'; ?>
		
		
		<?php } ?>
	</h3>
	<div class="btn-group pull-right">
			<?php if($url3 == "users"){ ?><a href="<?php echo site_url('/user/add');?>" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-plus"></i> User</a>
			<?php } else if($url3 == "groups"){ ?><a href="<?php echo site_url('/group/add');?>" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-plus"></i> Group</a>
			<?php } else if($url3 == "permissions"){ ?><a href="<?php echo site_url('/permission/add');?>" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-plus"></i> Permission</a>
			<?php } ?>
		<button type="button" class="btn btn-sm btn-flat btn-default" onclick="reload_table()">
			<i class="fa fa-refresh"></i> Refresh
		</button>
    </div>
</div><!-- /.box-header -->