<!-- Content Header (Page header) -->
<section class="content-header">
		<h1>
            <? 
            // pemberian nama otomatis pada heading 
            if($url2=="setting"){ echo lang('setting_heading_content'); }
            elseif($url2=="manage"){ echo lang('manage_heading_content'); }
            elseif($url2=="transaction"){ echo "Transaction"; }
            else{echo lang('dashboard_heading_content');}
            ?> 
            <small>
            <? 
            // pemberian nama otomatis pada heading ukuran kecil
            if($url2 == "manage"){
                if($url3=="users"){ echo lang('manage_subheading_content_users'); }
                elseif($url3=="groups"){ echo lang('manage_subheading_content_groups'); }
                elseif($url3=="permissions"){ echo lang('manage_subheading_content_permissions'); }
                elseif($url3=="bank"){ echo "Bank"; }
                elseif($url3=="hotel"){ echo "Hotel"; }
                elseif($url3=="commission"){ echo "Commission"; }
            }
            
            elseif($url2 == "transaction"){
                if($url3=="packet"){ echo "packet"; }
                elseif($url3=="commission"){ echo "commission"; }
            }

            
            elseif($url3=="general"){ echo lang('setting_general_subheading_content'); }
            elseif($url3=="seo"){ echo lang('setting_seo_subheading_content'); }
            elseif($url3=="socmed"){ echo lang('setting_socmed_subheading_content'); }
            else{echo lang('dashboard_heading_content_small');}
            ?> 
            </small> 
        </h1>
        <!-- Penetapan breadcrumb -->
		<ol class="breadcrumb">
            <li>
                <a href="<?php echo site_url('dashboard');?>">
                    <i class="fa fa-dashboard fa-fw"></i><? echo lang('dashboard_heading_content'); ?>
                </a>
            </li>
            <?php if($url3){ ?>
            <li>
                <a href="#">
                <? if($url2=="manage"){ ?><i class="fa fa-gears fa-fw"></i><? echo lang('manage_heading_content');?>
                <? }elseif($url2=="setting"){ ?><i class="fa fa-wrench fa-fw"></i><? echo lang('setting_heading_content');?>
                <? } ?>
                </a>
            </li>
            <li class="active">
                <? if($url3=="users"){ ?><i class="fa fa-users fa-fw"></i> <? echo lang('manage_heading_content_users');?>
                <? }elseif($url3=="groups"){ ?><i class="fa fa-th-large fa-fw"></i> <? echo lang('manage_heading_content_groups');?>
                <? }elseif($url3=="permissions"){ ?><i class="fa fa-flag fa-fw"></i> <? echo lang('manage_heading_content_permissions');?>
                <? }elseif($url3=="general"){ ?><i class="fa fa-universal-access fa-fw"></i> <? echo lang('setting_general_heading_content');?>
                <? }elseif($url3=="seo"){ ?><i class="fa fa-globe fa-fw"></i> <? echo lang('setting_seo_heading_content');?>
                <? }elseif($url3=="socmed"){ ?><i class="fa fa-share-alt fa-fw"></i> Social Media
                <? } ?>
            </li>
            <?php } ?>
        </ol>
</section>