<? 
    /**
     * 00 = ROOT; 
     * 11 = Admin1 - admin member; 
     * 12 = Admin2 - bagian reservasi; 
     * 13 = Admin3 - PO; 
     * 14 = Kurir; - Not Login
     * 15 = TM; - Not Login
     * 02 = member; 
     * 03 = administrator
     */


  $userType = $this->session->userdata('user_type');
  if($userType=="00"){$nameType = "ROOT";}
  elseif($userType=="11"){$nameType = "Administrator";}
  elseif($userType=="12"){$nameType = "Reservasi";}
  elseif($userType=="13"){$nameType = "Marketing";}
  elseif($userType=="02"){$nameType = "Member";}
  elseif($userType=="03"){$nameType = "Supervisor";}
  else{$nameType = "NULL";}
?>

<? if($userType=="03"){ /* For Supervisor */ ?>

<? }elseif($userType=="11"){ /* For Supervisor */?>
    
<? }elseif($userType=="12"){ /* For Reservation */?>
    
<? }elseif($userType=="13"){ /* For PO Marketing */?>
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-warning" style="opacity:0.9">
                <span class="h5 text-uppercase"><i class="fa fa-bullhorn fa-fw"></i> 
                <span class="badge"><? echo $num = $this->m_num_->get_num_packet_transaction_waiting();?></span> 
                    paket belum diselesaikan pembayarannya via 
                <i class="fa fa-credit-card fa-fw"></i> Kartu Kredit
                </span>
            </div>
        </div>
        <!-- /.col -->
    </div>
<? }elseif($userType=="02"){ /* For user member dan non-member */?>
    Halo member!
<? }else{ /* Other */?>
    Ada Kesalahan!
<? } ?>

    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <h4>Halo, <?php echo ucwords($this->session->userdata('full_name'));?></h4>
                <p>
                    Pada akun ini, Anda diberikan hak akses sebagai <? echo $nameType;?>...<br>
                    <small>
                        <i class="fa fa-calendar fa-fw"></i> <?php echo date("d/m/Y"); ?>
                        <i class="fa fa-clock-o fa-fw"></i> <span id="clock">&nbsp;</span>
                    </small>
                </p>
            </div>
        </div>
        <!-- /.col -->
    </div>

<? 
if($userType=="11"){
    /* For Supervisor */
    $this->load->view('su_copanel/component/pages/page-dashboard-11-administrator');
}elseif($userType=="12"){
    /* For Reservation */
    $this->load->view('su_copanel/component/pages/page-dashboard-12-reservation');
}elseif($userType=="13"){
    /* For PO Marketng */
    $this->load->view('su_copanel/component/pages/page-dashboard-13-marketing');
}elseif($userType=="02"){
    /* For Member dan non-member */
    $this->load->view('su_copanel/component/pages/page-dashboard-02-member');
}elseif($userType=="03"){
    /* For Supervisor */
    $this->load->view('su_copanel/component/pages/page-dashboard-03-supervisor');
} ?>