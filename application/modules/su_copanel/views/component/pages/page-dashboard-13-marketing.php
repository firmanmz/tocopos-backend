<!-- For PO Marketing -->
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-orange">
            <div class="inner">
                <h3><? echo $num = $this->m_num_->get_num_packet_transaction();?></h3>
                <p>Jumlah Pembelian Paket</p>
            </div>
            <div class="icon">
                <i class="fa fa-dropbox"></i>
            </div>
            <a href="<? echo site_url('tranpacket/add'); ?>" class="small-box-footer">
                <i class="fa fa-cart-plus fa-fw"></i> <strong>Beli Paket</strong>
            </a>
        </div>
    </div>

    <?php if($get_packet){ ?>
        <?php $no=0; foreach($get_packet as $gp){ ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3><? echo $num = $this->m_num_->get_num_packet_transaction($gp->id_packet);?></h3>
                        <p>Jumlah Pembelian <strong>Paket <?php echo $gp->packet_name;?></strong></p>
                    </div>
                    <div class="icon">
                        <span class="fa-stack" style="font-size:60px;">
                            <i class="fa fa-dropbox fa-stack-1x"></i>
                            <i class="fa fa-circle-o-notch fa-rotate-90 fa-stack-2x"></i>
                        </span>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
	    <?php } ?>
    <?php } else { ?>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>Kosong</h3>
                    <p>Tidak ada Packet yang tersedia</p>
                </div>
                <div class="icon">
                    <i class="fa fa-times"></i>
                </div>
                <a href="<? echo site_url('packet/add'); ?>" class="small-box-footer">Packet <i class="fa fa-plus fa-fw"></i></a>
            </div>
        </div>
    <?php } ?>
</div>