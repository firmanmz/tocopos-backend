<!-- get data ajax -->
<script type="text/javascript">
	function reload_table(){
		table.ajax.reload(null,false); /* reload datatable ajax  */
	}
	function reload_page(){
		location.reload(); /* reload page ajax  */
	}
	
/*<![CDATA[ */
	var save_method; /* for save method string */
    var table;
    $(document).ready(function() {
		table = $('#table').DataTable({
			"processing": true, /* Feature control the processing indicator., */
			"language": {
				"processing": "<img height='53' width='53' src='<?php echo base_url('assets/backend/base/adminlte/dist/img/load.gif'); ?>' />" /* add a loading image,simply putting <img src="loader.gif" /> tag. */
			},
			"serverSide": true, /* Feature control DataTables' server-side processing mode. */
			
			"paging": true,
			"lengthChange": true,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"stateSave": true,
			
			"order": [], /* Initial no order. */
			
			<?php 
				if($url3 == 'users' or 
				   $url3 == 'groups' or
				   $url3 == 'permissions' or
				   $url3 == 'bank' or 
				   $url3 == 'hotel' or
				   $url3 == 'commission' 
				)
			{?>
			/*
			"aoColumns": [
							<?php if($url3 == 'users'){?>{ "bSortable": false }, null, null, null, null
							<?php } else if($url3 == 'groups'){?>{ "bSortable": false }, null, null, null
							<?php } else if($url3 == 'permissions'){?>{ "bSortable": false }, null, null
							<?php } else {?>
							<?php } ?>
						],	
			*/		
			<?php } ?>
		
		
			/* Load data for the table's content from an Ajax source */
			"ajax": {
				
			<?php if($url3 == "users"){ ?> "url": "<?php echo base_url('c_data/c_users/ajax_list')?>",
			<?php } else if($url3 == "permissions"){ ?> "url": "<?php echo base_url('c_data/c_permissions/ajax_list')?>",
			<?php } else if($url3 == "groups"){ ?> "url": "<?php echo base_url('c_data/c_groups/ajax_list')?>",
			<?php } else if($url3 == "bank"){ ?> "url": "<?php echo base_url('c_data/c_manage_bank/ajax_list')?>",
			<?php } else if($url3 == "hotel"){ ?> "url": "<?php echo base_url('c_data/c_manage_hotel/ajax_list')?>",
			
			<?php } ?>
			
				"type": "POST",
				/* success: function(data) { alert("succsess") }, */
				/* error: function(ts) { alert(ts.responseText) } */
			},
			/* Set column definition initialisation properties. */
			"columnDefs": [
				{ 
					"targets": [ -1 ], /* last column */
					"orderable": false, /* set not orderable */
					"searchable": false,
					"orderable": false,
					"visible": true 
				},
			],
		});
    });

	
    function delete_person(id,name=null) {

        /* console.log(name); */
		<?php if($url3=="groups"){ ?>
                var title = "<?php echo lang('delete_group_confirmation_heading'); ?>";
                var text = "<?php echo lang('delete_group_confirmation_subheading'); ?>" + ' "' + name + '"';
		<?php	}elseif($url3=="permissions"){ ?>
                var title = "<?php echo lang('delete_permission_confirmation_heading'); ?>";
                var text = "<?php echo lang('delete_permission_confirmation_subheading'); ?>" + ' "' + name + '"';
		<?php	}elseif($url3=="bank"){ ?>
                var title = "<?php echo lang('delete_bank_confirmation_heading'); ?>";
                var text = "<?php echo lang('delete_bank_confirmation_subheading'); ?>" + ' "' + name + '"';
		<?php	}elseif($url3=="hotel"){ ?>
                var title = "<?php echo lang('delete_hotel_confirmation_heading'); ?>";
                var text = "<?php echo lang('delete_hotel_confirmation_subheading'); ?>" + ' "' + name + '"';
		<?php } ?>

		/* Konfirmasi yes no dengan PNotify */
        PNotify.desktop.permission();
        PNotify.prototype.options.styling = "bootstrap3";
        (new PNotify({
			title: title,
			text: text,
            icon: 'fa fa-question-circle fa-lg',
            hide: false,
            shadow: false,
            desktop: {
                desktop: false
            },
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            },
            addclass: 'stack-modal',
            stack: {
                'dir1': 'down',
                'dir2': 'right',
                'modal': true
            }
        })).get().on('pnotify.confirm', function() {            
			$.ajax({
			<?php if($url3 == "users"){ ?>url : "<?php echo base_url('c_data/c_users/ajax_delete')?>/"+id,
			<?php } else if($url3 == "permissions"){ ?>url : "<?php echo base_url('c_data/c_permissions/ajax_delete')?>/"+id,
			<?php } else if($url3 == "groups"){ ?>url : "<?php echo base_url('c_data/c_groups/ajax_delete')?>/"+id,
			<?php } else if($url3 == "bank"){ ?>url : "<?php echo base_url('c_data/c_manage_bank/ajax_delete')?>/"+id,
			<?php } else if($url3 == "hotel"){ ?>url : "<?php echo base_url('c_data/c_manage_hotel/ajax_delete')?>/"+id,
			<?php } ?>
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					/* if success reload ajax table */
					/* $('#modal_form').modal('hide'); */
					reload_table();
				   
					/* PNotify Desktop */
					PNotify.desktop.permission();
					PNotify.prototype.options.styling = "bootstrap3";
					(new PNotify({
						title: 'Hapus!',
						text: 'Hore, data telah berhasil dihapus.',
						type: 'success',
						desktop: {
							desktop: false
						}
					})).get().click(function(e) {
						if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
						alert('Hey! You clicked the desktop notification!');
					});
				},  
				error: function ()  /* error: function (jqXHR, textStatus, errorThrown) */
				{
					alert('Error deleting data');
				}
			});
        });
		
		/*
		//if(confirm('Are you sure delete this data?')){
			ajax delete data to database 
			console.log(id);
		}
		*/
    }
	
/* ]]> */
</script>