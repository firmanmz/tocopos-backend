	
	<!--<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>-->
	<script src="<?php echo base_url('assets/backend/plugins/jQuery/dist/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('assets/backend/plugins/jQueryUI/jquery-ui.min.js');?>"></script>
	<script src="<?php echo base_url('assets/backend/base/bootstrap/js/bootstrap.min.js');?>"></script>
	
	<!-- Nprogress -->
	<?php $this->load->view('su_copanel/footer/footer-other/other-nprogress'); ?>

	<!-- Slimscroll -->
	<script src="<?php echo base_url('assets/backend/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('assets/backend/plugins/fastclick/lib/fastclick.js');?>"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/backend/base/adminlte/dist/js/adminlte.min.js');?>"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo base_url('assets/backend/base/adminlte/dist/js/demo.js');?>"></script>

	<!-- master css -->
	<script src="<?php echo base_url('assets/backend/base/master.js');?>"></script>
	<?php $this->load->view('su_copanel/footer/footer-other/noscript'); ?>
	
  </body>
</html>