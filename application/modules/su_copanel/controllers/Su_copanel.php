<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Auth.php
 *
 * @package     CI-ACL
 * @author      Steve Goodwin
 * @copyright   2015 Plumps Creative Limited
 */
class Su_copanel extends CI_Controller{

    function __construct(){
        parent::__construct();
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        if( ! $this->ion_auth->logged_in() )
            redirect('/login');

		$this->url1 = $this->uri->rsegment(1);
		$this->url2 = $this->uri->rsegment(2);
		$this->url3 = $this->uri->rsegment(3);
		$this->url4 = $this->uri->rsegment(4);
		$this->url5 = $this->uri->rsegment(5);

		$user['user']=$this->ion_auth->user()->row();
		$this->id_user=$user['user']->id;
		$this->username=$user['user']->username;
		$this->full_name=$user['user']->full_name;
		
		$this->users_groups=$this->ion_auth->get_users_groups()->result();
    }

	public function index(){ redirect('dashboard'); }

	function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
		$url = 'https://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}
	
    public function language($language = "", $current = FALSE) {
        $language = ($language != "") ? $language : $this->config->item('language','config');
        $this->session->set_userdata('site_lang', $language);
		$url = decodeID($current); //decode uri_string ($current)
		redirect($url);
    }

	public function dashboard(){
		$id_decode = decodeID($this->id_user);	
		$user = $this->ion_auth->user($id_decode)->row();
		$user_type = $this->session->userdata('user_type');
		$data = array(
			'title' => lang('dashboard_title')."z",
			'id_user' => $this->id_user,
			'url2' => $this->url2,
			'url3' => $this->url3,
			'message' => '',
			//'users_groups' => $this->users_groups,
			//'users_permissions' => $this->ion_auth_acl->build_acl()
		);
		if($user_type=="13"){
			$data['get_packet'] = $this->m_manage_packet->get_packet();
		}elseif($user_type=="02"){
			// get data user member | ($id_user, $how_view)
			$data['get_users'] = $this->m_manage_users_member->get_users($this->id_user, 'r');
		}

		$data['user_data'] = $user;
		$this->_render_page('su_copanel/content/contents', 'su_copanel/component/pages/pages-content/content', $data);
	}

	function _render_page($view, $data=null, $returnhtml=false){ //I think this makes more sense
		$this->viewdata = (empty($data)) ? $data: $data;
		$view_html = $this->template->load($view, $this->viewdata, $returnhtml);
		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}
}
